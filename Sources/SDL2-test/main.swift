import SDL2

do {
	try SDL2.initialize(.video)
	defer {
		SDL2.quit()
	}
	let window = try Window(title: "Test", x: 0, y: 0, width: 640, height: 480, flags: [.fullScreenDesktop])

	var isExit = false

	while !isExit {
		while let event = Event.poll() {
			switch event {
			case .quit:
				isExit = true
			default:
				break
			}
		}
	}
} catch let error as SDLError {
	print(error.message)
}

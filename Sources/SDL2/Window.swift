import CSdl2

enum TempError: Error {
	case error
}

public class Window {

	internal let n_Window: OpaquePointer

	public init(title: String, x: Int, y: Int, width: Int, height: Int, flags: WindowFlags = []) throws {
		guard let n_Window = title.withCString({
			SDL_CreateWindow($0, x.sdl, y.sdl, width.sdl, height.sdl, flags.rawValue)
		}) else {
			throw SDLError()
		}
		self.n_Window = n_Window
	}

	deinit {
		SDL_DestroyWindow(n_Window)
	}

	public
	var borderSize: Indent {
		get {
			var top: Int32 = 0, left: Int32 = 0, bottom: Int32 = 0, right: Int32 = 0
			SDL_GetWindowBordersSize(n_Window, &top, &left, &bottom, &right)
			return Indent(top: top, left: left, bottom: bottom, right: right)
		}
	}

	public
	var flags: WindowFlags {
		return WindowFlags(rawValue: SDL_GetWindowFlags(n_Window))
	}

	public
	func getBrightness() -> Float {
		return SDL_GetWindowBrightness(n_Window)
	}

	// TODO: Get data

	public
	func getDisplayIndex() throws -> Int {
		let index = SDL_GetWindowDisplayIndex(n_Window)
		guard index >= 0 else {
			throw SDLError()
		}
		return Int(index)
	}

	public
	func getDisplayMode() throws -> DisplayMode {
		var n_DisplayMode = SDL_DisplayMode()
		guard SDL_GetWindowDisplayMode(n_Window, &n_DisplayMode) == 0 else {
			throw SDLError()
		}
		return DisplayMode(n_DisplayMode: n_DisplayMode)
	}

	// TODO: Get gamma ramp

	public
	func getId() throws -> Int {
		let id = SDL_GetWindowID(n_Window)
		guard id != 0 else {
			throw SDLError()
		}
		return Int(id)
	}

	public
	func getOpacity() throws -> Float {
		var opacity: Float = 0
		guard SDL_GetWindowOpacity(n_Window, &opacity) == 0 else {
			throw SDLError()
		}
		return opacity
	}

	public
	func getPixelFormat() throws -> PixelFormat {
		guard let pixelFormat = PixelFormat(rawValue: Int(SDL_GetWindowPixelFormat(n_Window))),
		      pixelFormat != .unknown else {
			throw SDLError()
		}
		return pixelFormat
	}

	public
	func getSurface() throws -> Surface {
		guard let n_Surface = SDL_GetWindowSurface(n_Window) else {
			throw SDLError()
		}
		return Surface(n_Surface: n_Surface)
	}

	public
	func hide() {
		SDL_HideWindow(n_Window)
	}

	public
	var isBordered: Bool {
		get {
			return flags.contains(.borderless)
		}
		set {
			SDL_SetWindowBordered(n_Window, newValue.sdl)
		}
	}

	public
	var isGrabbed: Bool {
		get {
			return SDL_GetWindowGrab(n_Window) == SDL_TRUE
		}
		set {
			SDL_SetWindowGrab(n_Window, newValue.sdl)
		}
	}

	public
	var isResizable: Bool {
		get {
			return flags.contains(.resizable)
		}
		set {
			SDL_SetWindowResizable(n_Window, isResizable.sdl)
		}
	}

	public
	func maximize() {
		SDL_MaximizeWindow(n_Window)
	}

	public
	var maximumSize: Size {
		get {
			var width: Int32 = 0
			var height: Int32 = 0
			SDL_GetWindowMaximumSize(n_Window, &width, &height)
			return Size(width: width, height: height)
		}
		set {
			SDL_SetWindowMaximumSize(n_Window, newValue.width.sdl, newValue.height.sdl)
		}
	}

	public
	func minimize() {
		SDL_MinimizeWindow(n_Window)
	}

	public
	var minimumSize: Size {
		get {
			var width: Int32 = 0
			var height: Int32 = 0
			SDL_GetWindowMinimumSize(n_Window, &width, &height)
			return Size(width: width, height: height)
		}
		set {
			SDL_SetWindowMinimumSize(n_Window, newValue.width.sdl, newValue.height.sdl)
		}
	}

	public
	var position: Position {
		get {
			var x: Int32 = 0
			var y: Int32 = 0
			SDL_GetWindowPosition(n_Window, &x, &y)
			return Position(x: x, y: y)
		}
		set {
			SDL_SetWindowPosition(n_Window, newValue.x.sdl, newValue.y.sdl)
		}
	}

	public
	func raise() {
		SDL_RaiseWindow(n_Window)
	}

	public
	func restore() {
		SDL_RestoreWindow(n_Window)
	}

	public
	func setBrightness(_ brightness: Float) throws {
		guard SDL_SetWindowBrightness(n_Window, brightness) == 0 else {
			throw SDLError()
		}
	}

	// TODO: Set data

	public
	func setDisplayMode(_ displayMode: DisplayMode?) throws {
		if var n_DisplayMode = displayMode?.sdl {
			guard SDL_SetWindowDisplayMode(n_Window, &n_DisplayMode) == 0 else {
				throw SDLError()
			}
		} else {
			guard SDL_SetWindowDisplayMode(n_Window, nil) == 0 else {
				throw SDLError()
			}
		}
	}

	public
	func setFullScreen(_ flags: WindowFullScreenFlags) throws {
		 guard SDL_SetWindowFullscreen(n_Window, flags.rawValue) == 0 else {
			 throw SDLError()
		 }
	}

	// TODO: Set gamma ramp

	public
	func setIcon(_ icon: Surface) {
		SDL_SetWindowIcon(n_Window, icon.n_Surface)
	}

	public
	func setInputFocus() throws {
		guard SDL_SetWindowInputFocus(n_Window) == 0 else {
			throw SDLError()
		}
	}

	public
	func setModal(for window: Window) throws {
		guard (SDL_SetWindowModalFor(n_Window, window.n_Window)) == 0 else {
			throw SDLError()
		}
	}

	public
	func setOpacity(_ opacity: Float) throws {
		guard SDL_SetWindowOpacity(n_Window, opacity) == 0 else {
			throw SDLError()
		}
	}

	public
	func show() {
		SDL_ShowWindow(n_Window)
	}

	public
	var size: Size {
		get {
			var width: Int32 = 0
			var height: Int32 = 0
			SDL_GetWindowSize(n_Window, &width, &height)
			return Size(width: width, height: height)
		}
		set {
			SDL_SetWindowSize(n_Window, newValue.width.sdl, newValue.height.sdl)
		}
	}

	public
	var title: String {
		get {
			return String(cString: SDL_GetWindowTitle(n_Window))
		}
		set {
			newValue.withCString {
				SDL_SetWindowTitle(n_Window, $0)
			}
		}
	}

	public
	func updateSurface() throws {
		guard SDL_UpdateWindowSurface(n_Window) == 0 else {
			throw SDLError()
		}
	}
}

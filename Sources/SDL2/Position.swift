public
struct Position {
    let x: Int
    let y: Int

    public
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }

    init(x: Int32, y: Int32) {
        self.x = Int(x)
        self.y = Int(y)
    }
}
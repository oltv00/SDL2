import CSdl2

public
class Surface {

	let n_Surface: UnsafeMutablePointer<SDL_Surface>

	init(n_Surface: UnsafeMutablePointer<SDL_Surface>) {
		self.n_Surface = n_Surface
	}
}

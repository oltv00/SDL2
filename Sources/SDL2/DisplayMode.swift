import CSdl2

public
struct DisplayMode {
	public
	let pixelFormat: PixelFormat

	public
	let size: Size

	public
	let refreshRate: Int

	public
	let driverData: UnsafeRawPointer

	init(n_DisplayMode: SDL_DisplayMode) {
		let (width, height) = (n_DisplayMode.w, n_DisplayMode.h)

		self.size = Size(width: width, height: height)
		self.pixelFormat = PixelFormat(rawValue: Int(n_DisplayMode.format)) ?? .unknown
		self.refreshRate = Int(n_DisplayMode.refresh_rate)
		self.driverData = UnsafeRawPointer(n_DisplayMode.driverdata)
	}

	var sdl: SDL_DisplayMode {
		var n_DisplayMode = SDL_DisplayMode()
		n_DisplayMode.w = size.width.sdl
		n_DisplayMode.h = size.height.sdl
		n_DisplayMode.format = UInt32(pixelFormat.rawValue)
		n_DisplayMode.refresh_rate = refreshRate.sdl
		n_DisplayMode.driverdata = UnsafeMutableRawPointer(mutating: driverData)
		return n_DisplayMode
	}
}